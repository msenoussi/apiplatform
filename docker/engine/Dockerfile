FROM php:7.3-fpm

WORKDIR /home/docker

ARG PHP_APCU_VERSION=5.1.17
ARG PHP_IMAGICK_VERSION=3.4.3
ARG PHP_XDEBUG_VERSION=2.7.2

RUN apt update \
    && apt install -y \
    git \
    libfreetype6-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libmagickwand-dev \
    libpq-dev \
    libzip-dev \
    libxml2-dev \
    zlib1g-dev \
    libssl-dev \
    xfonts-75dpi \
    xfonts-base \
    p7zip-full \
    && docker-php-source extract \
    && curl -L -o /tmp/wkhtmltopdf.deb https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb \
    && dpkg -i /tmp/wkhtmltopdf.deb \
    && curl -L -o /tmp/apcu-$PHP_APCU_VERSION.tgz https://pecl.php.net/get/apcu-$PHP_APCU_VERSION.tgz \
    && curl -L -o /tmp/imagick-$PHP_IMAGICK_VERSION.tgz https://pecl.php.net/get/imagick-$PHP_IMAGICK_VERSION.tgz \
    && curl -L -o /tmp/xdebug-$PHP_XDEBUG_VERSION.tgz http://xdebug.org/files/xdebug-$PHP_XDEBUG_VERSION.tgz \
    && tar xfz /tmp/apcu-$PHP_APCU_VERSION.tgz \
    && tar xfz /tmp/imagick-$PHP_IMAGICK_VERSION.tgz \
    && tar xfz /tmp/xdebug-$PHP_XDEBUG_VERSION.tgz \
    && rm -r \
    /tmp/apcu-$PHP_APCU_VERSION.tgz \
    /tmp/imagick-$PHP_IMAGICK_VERSION.tgz \
    /tmp/xdebug-$PHP_XDEBUG_VERSION.tgz \
    && mv apcu-$PHP_APCU_VERSION /usr/src/php/ext/apcu \
    && mv imagick-$PHP_IMAGICK_VERSION /usr/src/php/ext/imagick \
    && mv xdebug-$PHP_XDEBUG_VERSION /usr/src/php/ext/xdebug \
    && docker-php-ext-install -j$(nproc) \
    apcu \
    gd \
    imagick \
    intl \
    mbstring \
    opcache \
    pdo_pgsql \
    xml \
    xdebug \
    simplexml \
    zip \
    && docker-php-source delete

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

COPY ./php.ini /usr/local/etc/php/conf.d/custom.ini

ENTRYPOINT ["docker-entrypoint"]
CMD ["php-fpm"]